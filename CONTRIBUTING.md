# Contributing to the Project

Thank you for considering contributing to this project! Here are a few guidelines to follow:

## How to Contribute

1. **Fork the repository** on GitLab.
2. **Clone your fork** locally:
    ```sh
    git clone https://gitlab.com/username/laravel-api-boilerplate.git
    ```
3. **Create a branch** for your feature or fix:
    ```sh
    git checkout -b my-new-feature
    ```
4. **Commit your changes** with a descriptive commit message:
    ```sh
    git commit -am 'Add some feature'
    ```
5. **Push to the branch**:
    ```sh
    git push origin my-new-feature
    ```
6. **Create a Merge Request** on GitLab.

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md).
