# Laravel API Boilerplate

## Overview

This is a boilerplate project for building a RESTful API using Laravel 11.x and Laravel Sanctum for authentication. It
includes basic features such as user registration, login, password change, and retrieving user information. The project
is set up with CI/CD pipelines using GitLab and supports Auto DevOps with Kubernetes.

## Features

- User Registration
- User Login
- Password Change
- Retrieve User Information
- Token-based Authentication with Laravel Sanctum
- CI/CD with GitLab
- Auto DevOps and Kubernetes support

## Installation

### Prerequisites

- PHP 8.0 or higher
- Composer
- MySQL or another supported database
- Git

### Steps

1. Clone the repository:
```sh
   git clone https://gitlab.com/your-username/laravel-api-boilerplate.git
   cd laravel-api-boilerplate
```

2. Install dependencies:
```sh
   composer install
```

3. Create a new `.env` file:
```sh
   cp .env.example .env
```

4. Generate an application key:
```sh
   php artisan key:generate
```

5. Update the `.env` file with your database credentials and other settings.

6. Run the database migrations:
```sh
   php artisan migrate
```

7. Start the development server:
```sh
   php artisan serve
```

---

### Usage

#### Register

##### Endpoint: POST /api/register

##### Body:

```json
{
    "name": "John Doe",
    "email": "john.doe@example.com",
    "password": "password123",
    "password_confirmation": "password123"
}
```

#### Login

##### Endpoint: POST /api/login

##### Body:

```json
{
    "email": "john.doe@example.com",
    "password": "password123"
}
```

#### Login

##### Endpoint: POST /api/logout

##### Headers:

```
{
    "Authorization": "Bearer {your_access_token}"
}
```

#### Change Password

##### Endpoint: POST /api/change-password

##### Headers:

```json
{
    "Authorization": "Bearer {your_access_token}"
}
```

##### Body:

```json
{
    "current_password": "password123",
    "new_password": "newpassword123",
    "new_password_confirmation": "newpassword123"
}
```

#### Get Authenticated User

##### Endpoint: GET /api/me

##### Headers:

```json
{
    "Authorization": "Bearer {your_access_token}"
}
```

---

#### CI/CD with GitLab

This project includes a .gitlab-ci.yml file for setting up CI/CD pipelines with GitLab. The pipeline includes stages for
testing and deployment.

#### License

This project is licensed under the MIT License. See the LICENSE file for details.

#### Contributing

Please read CONTRIBUTING for details on our code of conduct, and the process for submitting pull requests.

#### Changelog

See the CHANGELOG file for details.

#### Support

If you have any questions or need support, please open an issue in the GitLab repository.
