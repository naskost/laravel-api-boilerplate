# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

- Initial setup with Laravel, Sanctum, and basic authentication.

### Changed

### Fixed

## [1.0.0] - YYYY-MM-DD

- Initial release.
